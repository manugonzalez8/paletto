import { shallowMount } from '@vue/test-utils'
import Pallete from '@/components/Pallete.vue'

describe('Pallete.vue', () => {
  it('Renderizar los modos de color', () => {
    const msg = 'HEX RGB HSL'
    const wrapper = shallowMount(Pallete)
    expect(wrapper.text()).toMatch(msg)
  });
/*
  it('Por cada elemento del arreglo de colores pasado a la paleta debe existir un botón asociado al mismo con un color de fondo equivalente al codeHex', () => {
    expect('🤮').toBe('👊');
  });
*/
  it('Al hacer click a un color su valor hex se imprime en el input establecido', () => {
    const wrapper = shallowMount(Pallete,{
      propsData: {
        colors: [
          {
              "name": "Cool Blue",
              "codeHex": "#3498db"
          },
      ]
      }
    });
    const buttonWrapper = wrapper.find('.color');
    buttonWrapper.trigger('click');
    expect(wrapper.vm.$data.selectedColor.value).toMatch("#3498db");
  });

 /* it('Al hacer click al modo de color RGB después de haber seleccionado un color, el valor del input debe corresponder a la conversión del valor Hex a RGB', () => {
    expect('🤮').toBe('👊');
  });*/

  // Nuevas funcionalidades a desarrollar (acá es donde ustedes escriben las pruebas antes de hacer el feature)
 /* it('Al hacer click al modo de color HSL después de haber seleccionado un color, el valor del input debe corresponder a la conversión del valor Hex a HSL', () => {
    expect('🤮').toBe('👊');
  });*/

})
